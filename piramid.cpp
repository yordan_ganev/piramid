/*
 *  Author: Yordan
 *  Date:   09.10 2020
 *
 *  About: Piramid structure order library
 */

// --- Libraries ---
#include <iostream>
#include <string>
#include <string.h>
#include "piramid.h"
#include <cstdlib>

using namespace std;

// --- Definitions ---

// clear console command
#ifdef __linux__
#define CLEAR "clear"
#else
#define CLEAR "cls"
#endif

int main()
{
    CPiramid<float> piramid;

    // Get nummbers
    char cmd = 0;

    while ( cmd != '0')
    {

        string input;

        // Selection menu

        cout << "\n\t--- Select option: ---\n";
        cout << "1. Enter number.\n";
        cout << "2. Pop max element.\n";
        cout << "0. Exit\n";

        getline(cin, input);

        // clear console
        system(CLEAR);

        // get input command
        cmd = input[0];

        switch (cmd)
        {
        case '1':
            cout << "Enter number: ";
            getline(cin, input);
            piramid.push(stof(input));
            break;
        case '2':
            cout << "Top poped!" << endl;
            piramid.pop();

            break;
        case '0':
            cout << "Exiting.." << endl;
            break;

        default:
            printf("Unexpected command! '%c' is not and option!\n", cmd);
            break;
        }

        // display items max element
        cout << "Max element : " << piramid.top() << endl;
    }

    return 0;
}
