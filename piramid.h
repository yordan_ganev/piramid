#ifndef PIRAMID_H
#define PIRAMID_H
// --- Libraries ---
#include <inttypes.h>

// --- Definitions ---
#define PIRAMID_INIT_SIZE   3
#define PIRAMID_TOP         0
#define EMPTY               0

template <class T>
class CPiramid
{
private:
    T* items;
    uint32_t _maxSize;
    uint32_t _currentSize;

    // Private function prototypes
    T* _growItemsArray();
    void _emergeItem(const uint32_t& index);
    void _sinkItem(const uint32_t & index);


public:
    CPiramid();
    ~CPiramid();

    // Public function prototypes
    void push(const T& data);
    void pop ();
    T top ();

};

// Basic constructior
template <class T>
CPiramid<T>::CPiramid ()
    : _maxSize(PIRAMID_INIT_SIZE), _currentSize(PIRAMID_INIT_SIZE)
{
    // init piramid dynamic array
    items = new T[_maxSize];
}

// Destructor
template <class T>
CPiramid<T>::~CPiramid()
{
    delete [] items;
};


/**
 * @brief Push element to the piramid on first emply cell
 * and emerge it to it's place in the piramid
 * @param data: element's value
 */
template <class T>
void CPiramid<T>::push(const T& data)
{
    uint32_t lastIndex = 0;

    while (items[lastIndex] != 0) {
        lastIndex++;
        if ( lastIndex >= _maxSize)
        {
            items = _growItemsArray();
        }
    }

    items[lastIndex] = data;
    _emergeItem(lastIndex);

    _currentSize++;
}


/**
 * @brief Delete piramid top element and put next max element on top
 */
template <class T>
void CPiramid<T>::pop ()
{
    uint32_t last_index = _maxSize;
    while ( items[--last_index] == 0)
    {
        if (last_index == PIRAMID_TOP)
            break;
    }

    items[PIRAMID_TOP] = items[last_index];
    items[last_index] = EMPTY;

    _sinkItem(PIRAMID_TOP);

    _currentSize--;
}

/**
 * @return top element ( with max value ) of piramid
 */
template <class T>
T  CPiramid<T>::top ()
{
    return items[0];
}

/**
 * @brief Has two clidren: If left child or right child is higher child
 * is compared to parent.
 * Has only child: Child is compared to parent.
 * Has no children: Exit.
 * If child has child has higher value swap indexes with parent.
 * @param index: Current parent index
 */
template <class T>
void CPiramid<T>::_sinkItem(const uint32_t & index)
{
    // Calculate children indexes
    const uint32_t leftChild = index*2+1;
    const uint32_t rightChild = index*2+2;

    // Find Higher value child if they exist
    uint32_t higherChild;

    if ( leftChild > _maxSize  && rightChild > _maxSize)
    {
        return; // have no children
    }
    else if ( items[leftChild] == EMPTY || items[rightChild] == EMPTY)
    {
        // have no children
        if (items[leftChild] == EMPTY && items[rightChild] == EMPTY)
        {
            return;
        }


        if (items[leftChild] == EMPTY)
        {
            higherChild = rightChild; // have only right child
        }
        else
        {
            higherChild = leftChild; // have only left child
        }
    }
    else {
        // have two children
        if ( items[leftChild] > items[rightChild])
        {
            higherChild = leftChild;
        }
        else {
            higherChild = rightChild;
        }
    }

    if ( higherChild < _maxSize && items[index] < items[higherChild])
    {
        T tmp = items[index];
        items[index] = items[higherChild];
        items[higherChild] = tmp;

        // call the item by its new index
        _sinkItem(higherChild);
    }
}

/**
 * @brief Create array for piramid (max size + lastRow * 2) or
 * if array not init at start (max size * 2 + 3). Paste last
 * array into new one. Delete last.
 * @return New array with size for +1 row
 */
template <class T>
T* CPiramid<T>::_growItemsArray()
{
    // new size
    uint32_t newSize = ((_maxSize-1)*2 + 3);
    T* newItems = new T[newSize];


    // check alloc success
    if (newItems == nullptr)
    {
        return items;
    }

    // Copy items into new array and init empty values
    for (uint32_t item = 0; item < newSize; item++)
    {
        if (item < _maxSize)
        {
            newItems[item] = items[item];
        }
        else
        {
            newItems[item] = EMPTY;
        }
    }

    delete[] items;

    _maxSize = newSize;
    return newItems;
}


/**
 * @brief New item added to the piramid bottom row.
 * Swap if item has higher value than its parent.
 * Stop if not parent is higher or reached index 0.
 * @param index: current index of added element
 */
template <class T>
void CPiramid<T>::_emergeItem(const uint32_t& index)
{
    // stop when item reaches top
    if (index == PIRAMID_TOP)
    {
        return;
    }

    // find top item index
    uint32_t top;
    if ( index % 2 )
    {
        top = (index-1)/2; // odd index - left child
    }
    else
    {
        top = (index-2)/2; // even index - right child
    }

    // swap places with the top item
    // if new item has higher value
    if ( items[index] > items[top] )
    {
        T tmp = items[index];
        items[index] = items[top];
        items[top] = tmp;

        /// call the item by its new index
        _emergeItem(top);
    }
}

#endif // PIRAMID_H
